package evaltableaux;

public class E1_Q5 {
     
    public static void main(String[] args) {
        
          // Le tableau tempDec contient les températures relevées au mois de Décembre 2015
          // Le poste d'indice 0 contient la température du 1er Décembre ( 3°)
          // Le poste d'indice 1 contient la température du   2 Décembre ( 4°)
          // et ainsi de suite.        
          
          int[ ]   tempDec= {3, 4, 7, 5, -2, -3, 0, -4, -8, -8, -6, -3,  0, 4,  3,
                             2, 5, 4, 5,  6,  6, 5,  6,  6,  8, 10, 12, 10, 9, 10, 8};
       
          float min=0.0f, max=0.0f;
          for (int i=0; i<tempDec.length; i++) { 
              if (tempDec[i]>max){
                  max=tempDec[i];
                  
              }
              if (tempDec[i]<min){
                  min=tempDec[i];
              }
          }
          System.out.println("Température minimale du mois de Décembre 2015:"+min+"°");
          System.out.println("Température maximale du mois de Décembre 2015:"+max+"°");
          // Ecrire ici le code java permettant de répondre à la question
     
          
    }  
}
